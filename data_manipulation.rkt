#lang racket

(provide (all-defined-out))
;Functions for rearranging and transforming data.

;Transpose a list of lists similar to the way that t() does in R.
;Consider making this accomodate other data types for input. For
;example, a list of vectors.
;  lol = list of lists
;  il = inner list
(define (transpose_lol lol)
  (for/list ((i (length (list-ref lol 0))))
    (for/list ((il lol))
      (list-ref il i))))


; Sort a list of lists by a column specified by index number.
;Got this from Stack Overflow and slightly modified it.
; https://stackoverflow.com/questions/40050677/is-there-a-way-to-sort-a-list-of-lists-using-an-index
(define (sort-lol lol column-i)  ;;add comparitor as a parameter and use below.
  (sort lol
        (lambda(a b)
          (if (string<?  (list-ref a column-i)  (list-ref b column-i))
              #t #f))))

