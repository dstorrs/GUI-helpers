This package contains items to make GUI creation easier and more
complete for less effort.  The idea is to provide as much as possible
by default and require minimal information to create additional
elements.  It is very much a work in progress, as you can see from
comments in the code.  The test code contains examples.


For example, if you have a frame:

(define my-frame (new frame% 
                  [label "my-frame"]
                  [width 800]
                  [height 600]
                  ))

Then you can use the make-mac-menus with no optional parameters:

(make-mac-menus my-frame)

This will give you default menu items with standard keyboard shortcuts
that are universal to all GUI applications on macOS.  These include a
Quit menu item with a command-q keyboard shortcut,  "Close Window"
with command-w, Hide with command-h and Hide Others with
option-command-h.  I find myself using this frequently during development, just because I
want these basic shortcuts.

Now, this time, add some optional paramenters.

(make-mac-menus my-frame
                #:file-open file-open-actions
                #:file-save file-save-actions
                #:file-save-as file-save-as-actions
                #:edit #t ;Edit menu based on the default editor.
                #:font #t ;Font menu based on the default editor.
				)

This is intended to make the structure of the menus as obvious as
possible from reading the code.  You will have whatever menus are
specified and standard keyboard shortcuts are included by default
where possible.

As time goes on, additional menus and menu options will be added as
well as support for Windows and Linux.

This package is also intended to supply simple functions to create GUI
elements, such as tables, given data that is structured a certain way.
At this point, there are only two.  One just adds a message to a tab
panel.  The other creates a table from a list of hashes.
